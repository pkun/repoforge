include Rules.make

VERTITLE := $(TITLE)-$(VERSION)
SUBDIRS  :=
DATE     := $(shell date +'%Y%m%d')
ARCHIVE  := "$(TITLE)-$(DATE).tar.bz2"
RELEASE  := "$(VERTITLE).tar.bz2"

all:
	@for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir $(@) || exit 1; \
	done;

clean:
	$(RM) *.spec *.tar.bz2
	rm -f $(SCRIPTS_DIR)/$(TITLE)-config
	@for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir $(@) || exit 1; \
	done;

install: all
	@for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir install || exit 1; \
	done;
	# Install scripts and other stuff
	install -d $(INSTALL_SBIN_DIR)
	install -m 0755 $(SCRIPTS_DIR)/rfa $(INSTALL_SBIN_DIR)
	# Install configs
	install -d $(INSTALL_CONF_DIR)
	test -e $(INSTALL_CONF_DIR)/rfa.conf || install -m 0644 $(CONF_DIR)/rfa.conf $(INSTALL_CONF_DIR)

archive: clean
	ln -sf ./ $(VERTITLE)
	tar -cj --exclude $(VERTITLE)/$(VERTITLE) --exclude .svn --exclude CVS --exclude $(ARCHIVE) -f $(ARCHIVE) $(VERTITLE)/*
	$(RM) $(VERTITLE)

release: archive
	mv -f $(ARCHIVE) $(RELEASE)

.PHONY: all clean libs utils tests install po archive
