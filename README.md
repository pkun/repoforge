# RepoForge project


## About

The repoforge stands for Repository Forge.
It's admin tool to easy control GIT and SVN repositories with SSH access. The
permissions are based on UNIX groups and real users.

The project was initially created to support Subversion repositories with RPM
package sources for RPM-based Linux distribution. It has potentialities to use
different version control systems and generate different types of packages
(RPM, deb) using the same source code and patches. But this features were not
implemented yet.

The repoforge has the abilities to administer a SVN server with multiply projects
in easy way. Each project has the list of users who have read-only access and the
list of users with read-write permissions. The users is system users. The access
lists is simply unix groups. The repoforge has utility rfa to create, delete and
administer Subversion repositories.


## Resources

Homepage : https://repoforge.libcode.org

Repository : https://src.libcode.org/pkun/repoforge

Download: https://src.libcode.org/download/repoforge/

Last release: https://src.libcode.org/download/repoforge/repoforge-0.5.2.tar.xz

Author : Serj Kalichev `<pkun(_at_)libcode.org>`
