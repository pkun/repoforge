# $Id: Rules.make 532 2008-04-07 11:57:01Z pkun $

TITLE := repoforge
MAJOR := 0
MINOR := 6
BUGFIX := 0
VERSION := $(MAJOR).$(MINOR).$(BUGFIX)

SCRIPTS_DIR		:= scripts
CONF_DIR		:= conf

TARGET_PREFIX_DIR	:= /usr
TARGET_DATA_DIR		:= $(TARGET_PREFIX_DIR)/share
TARGET_BIN_DIR		:= $(TARGET_PREFIX_DIR)/bin
TARGET_SBIN_DIR		:= $(TARGET_PREFIX_DIR)/sbin
TARGET_PC_DIR		:= $(TARGET_DATA_DIR)/pkgconfig
TARGET_CONF_DIR		:= /etc/repoforge

DESTDIR			:= /
INSTALL_FAKE_ROOT	:= $(DESTDIR)
INSTALL_PREFIX_DIR	:= $(INSTALL_FAKE_ROOT)$(TARGET_PREFIX_DIR)
INSTALL_DATA_DIR	:= $(INSTALL_FAKE_ROOT)$(TARGET_DATA_DIR)
INSTALL_BIN_DIR		:= $(INSTALL_FAKE_ROOT)$(TARGET_BIN_DIR)
INSTALL_SBIN_DIR	:= $(INSTALL_FAKE_ROOT)$(TARGET_SBIN_DIR)
INSTALL_PC_DIR		:= $(INSTALL_FAKE_ROOT)$(TARGET_PC_DIR)
INSTALL_CONF_DIR	:= $(INSTALL_FAKE_ROOT)$(TARGET_CONF_DIR)

ifdef DEBUG
DEBUG_FLAGS = -DDEBUG -ggdb -gdwarf-2 -g3
OPT_FLAGS =
else
OPT_FLAGS = -O2
endif

CC	:= gcc
ARFLAGS	:= rcs
CFLAGS	+= $(OPT_FLAGS) \
    -std=c99 -fno-common \
    -Wstrict-prototypes -Wmissing-prototypes -Wsign-compare -Wmissing-declarations \
    -Wshadow -Wpointer-arith -Wcast-align -Wpacked -Wall -Werror $(DEBUG_FLAGS)
CXXFLAGS += $(OPT_FLAGS) \
    -fno-common -Wsign-compare \
    -Wshadow -Wpointer-arith -Wcast-align -Wpacked -Wall -Werror $(DEBUG_FLAGS)
CPPFLAGS += -MD -D_GNU_SOURCE
LDFLAGS	+= -z defs 

# Shared libraries specific CFLAGS
CFLAGS_SO := -fpic
